<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class IpsumFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ipsum';
    }
}
