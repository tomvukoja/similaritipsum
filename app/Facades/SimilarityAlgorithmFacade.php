<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SimilarityAlgorithmFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'similarity_algorithm';
    }
}
