<?php

namespace App\Services\SimilarityAlgorithm;

use App\Interfaces\SimilarityAlgorithm\SimilarityAlgorithmInterface;

class Algorithm
{
    private array $algorithms = [];

    public function __construct()
    {
        $this->registerProvider(new Levenshtein);
        $this->registerProvider(new Oliver());
    }

    public function registerProvider(SimilarityAlgorithmInterface $provider)
    {
        $this->algorithms[$provider->getName()] = ['provider' => $provider];
    }

    /**
     * Returns calculated similarity between provided texts for each available algorithm
     *
     * @param [type] ...$texts
     * @return array
     */
    public function getSimilarity(string ...$texts): array
    {
        $similarities = [];

        if (empty($this->algorithms)) return [];

        foreach ($this->algorithms as $algorithmName => $algorithm) {
            foreach ($texts as $key => $text) {
                // Get text & key which will be compared to current text in the iteration 
                // (e.g. the next element in the array if available if not the first element in the array)
                $nextText = ($texts[$key + 1]) ?? $texts[0];
                $nextKey = isset($texts[$key + 1]) ? $key + 1 : 0;

                // Store calculated similarities with information which texts were compared (e.g. text_0_text_1)
                $similarities[$algorithmName]["text{$key}_text{$nextKey}"] = $algorithm['provider']->getSimilarity($text, $nextText);
            };
        }

        return $similarities;
    }
}
