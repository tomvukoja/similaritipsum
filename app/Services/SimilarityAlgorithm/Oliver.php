<?php

namespace App\Services\SimilarityAlgorithm;

use App\Interfaces\SimilarityAlgorithm\SimilarityAlgorithmInterface;

class Oliver implements SimilarityAlgorithmInterface
{
    const NAME = 'oliver';

    /**
     * Gets algorithm name
     *
     * @return void
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Gets similarity percentage
     *
     * @param integer $amount
     * @return float
     */
    public function getSimilarity(string $string1, string $string2): float
    {
        similar_text($string1, $string2, $percentage);
        return round($percentage, 2);
    }
}
