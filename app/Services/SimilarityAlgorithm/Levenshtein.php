<?php

namespace App\Services\SimilarityAlgorithm;

use App\Interfaces\SimilarityAlgorithm\SimilarityAlgorithmInterface;

class Levenshtein implements SimilarityAlgorithmInterface
{
    const NAME = 'levenshtein';

    /**
     * Gets algorithm name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Gets similarity percentage
     *
     * @param integer $amount
     * @return float
     */
    public function getSimilarity(string $string1, string $string2): float
    {
        $string1 = str_split($string1, 200);
        $string2 = str_split($string2, 200);

        $stringSimilarity = [];

        foreach ($string1 as $key => $string) {
            if (isset($string2[$key])) $stringSimilarity[] = levenshtein($string, $string2[$key]);
        }

        return ceil(array_sum($stringSimilarity) / count($stringSimilarity));
    }
}
