<?php

namespace App\Services\Ipsum;

use App\Interfaces\Ipsum\IpsumInterface;
use App\Services\Ipsum\Providers\BaconIpsum;
use App\Services\Ipsum\Providers\LoremIpsum;
use RuntimeException;

class Ipsum
{
    private array $ipsumProviders = [];

    public function __construct()
    {
        $this->registerProvider(new LoremIpsum());
        $this->registerProvider(new BaconIpsum());
    }

    public function registerProvider(IpsumInterface $provider)
    {
        $this->ipsumProviders[$provider->getName()] = ['provider' => $provider];
    }

    private function checkProvider($providerName)
    {
        if (!isset($this->ipsumProviders[$providerName])) {
            throw new RuntimeException(sprintf('Unknown ipsum provider %s.', $providerName));
        }
    }

    /**
     * Gets provider text
     *
     * @param [type] $providerName
     * @param [type] $amount
     * @return string
     */
    public function getText($providerName, $amount): string
    {
        $this->checkProvider($providerName);

        return $this->ipsumProviders[$providerName]['provider']->getText($amount);
    }

    /**
     * Gets a list of currently registered providers
     *
     * @return array
     */
    public function getAllowedProviders(): array
    {
        return array_keys($this->ipsumProviders);
    }
}
