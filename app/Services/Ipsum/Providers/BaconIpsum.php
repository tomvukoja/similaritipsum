<?php

namespace App\Services\Ipsum\Providers;

use App\Clients\Ipsum\IpsumClient as IpsumIpsumClient;
use App\Interfaces\Ipsum\IpsumInterface as IpsumIpsumInterface;
use Illuminate\Support\Facades\Cache;

class BaconIpsum extends BaseIpsum implements IpsumIpsumInterface
{
    // Provider name
    const NAME = 'bipsum'; 

    // Provider base uri
    const URI = "//baconipsum.com/api";

    // Provider data types
    const TYPE_ALL_MEAT = 'all-meat';
    const TYPE_MEAT_AND_FILLER = 'meat-and-filler';

    /**
     * Gets provider name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Gets API URI
     *
     * @return string
     */
    public function getUri(): string
    {
        return self::URI;
    }

    /**
     * Gets the ipsum text
     * 
     * Returns the cached data if available, otherwise fetches the fresh one
     *
     * @param integer $amount
     * @return string
     */
    public function getText(int $amount = 10, string $type = self::TYPE_MEAT_AND_FILLER): string
    {
        $cacheKey = $this->getCacheKey([
            'no_of_paragraphs' => $amount,
            'type' => $type,
        ]);

        return Cache::rememberForever($cacheKey, function () use ($amount, $type) {
            $client = new IpsumIpsumClient($this->getUri());

            $response = $client->get([
                'paras' => $amount,
                'type' => $type,
                'format' => 'json'
            ]);

            if ($response->getStatusCode() === 200) {

                $ipsumText = json_decode($response->getBody());

                return implode('', $ipsumText);
            } else {
                return null;
            }
        });
    }
}
