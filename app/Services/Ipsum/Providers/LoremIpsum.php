<?php

namespace App\Services\Ipsum\Providers;

use App\Clients\Ipsum\IpsumClient as IpsumIpsumClient;
use App\Interfaces\Ipsum\IpsumInterface as IpsumIpsumInterface;
use Illuminate\Support\Facades\Cache;

class LoremIpsum extends BaseIpsum implements IpsumIpsumInterface
{
    // Provider name
    const NAME = 'lipsum';

    // Provider base uri
    const URI = "//lipsum.com/feed/json";

    // Provider data types
    const TYPE_WORDS = 'words';
    const TYPE_PARAGRAPHS = 'paras';
    const TYPE_BYTES = 'bytes';

    /**
     * Gets provider name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Gets API URI
     *
     * @return string
     */
    public function getUri(): string
    {
        return self::URI;
    }

    /**
     * Gets the ipsum text
     * 
     * Returns the cached data if available, otherwise fetches the fresh one
     *
     * @param integer $amount
     * @return string
     */
    public function getText(int $amount = 10, string $type = self::TYPE_PARAGRAPHS): string
    {
        $cacheKey = $this->getCacheKey([
            'no_of_paragraphs' => $amount,
            'type' => $type,
        ]);

        return Cache::rememberForever($cacheKey, function () use($amount, $type) {
            $client = new IpsumIpsumClient($this->getUri());

            $response = $client->get([
                'amount' => $amount,
                'what' => $type
            ]);

            if ($response->getStatusCode() === 200) {

                $responseData = json_decode($response->getBody());
                $feed = $responseData->feed;

                return $feed->lipsum;
            } else {
                return null;
            }
        });
    }
}
