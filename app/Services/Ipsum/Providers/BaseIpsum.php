<?php

namespace App\Services\Ipsum\Providers;

class BaseIpsum
{
    /**
     * Generate cache key
     * 
     * Class name + key/value combination for the provided args
     *
     * @param array $args
     * @return string
     */
    public function getCacheKey(array $args): string
    {
        return strtolower(class_basename(get_class($this))) . '_' . implode(',', array_map(
            function ($argumentValue, $argumentKey) {
                return sprintf("%s=%s", $argumentKey, $argumentValue);
            },
            $args,
            array_keys($args)
        ));
    }
}
