<?php

namespace App\Interfaces\SimilarityAlgorithm;

interface SimilarityAlgorithmInterface
{
    /**
     * Gets algorithm name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Gets similarity percentage
     *
     * @param integer $amount
     * @return float
     */
    public function getSimilarity(string $string1, string $string2): float;
}
