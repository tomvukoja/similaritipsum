<?php

namespace App\Interfaces\Ipsum;

interface IpsumInterface
{
    /**
     * Gets provider name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Gets API URI
     *
     * @return string
     */
    public function getUri(): string;

    /**
     * Gets the ipsum text 
     *
     * @param integer $amount
     * @return string
     */
    public function getText(int $amount): string;
}
