<?php

namespace App\Clients\Ipsum;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class IpsumClient
{
    protected $client;

    public function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }

    public function get(array $queryArgs = [])
    {
        try {
            return $this->client->request("GET", '', [
                'query' => $queryArgs
            ]);
        } catch (ClientException $e) {
            return $e->getResponse();
        }
    }
}
