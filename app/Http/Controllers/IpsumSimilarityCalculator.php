<?php

namespace App\Http\Controllers;

use App\Facades\IpsumFacade;
use App\Facades\SimilarityAlgorithmFacade;
use Illuminate\Http\JsonResponse;

class IpsumSimilarityCalculator extends Controller
{
    /**
     * Returns calculated similarity for ipsum texts with similarity algorithms
     *
     * @return JsonResponse
     */
    public function calculateSimilarity(): JsonResponse
    {
        // Get defined providers
        $providers = request()->get('providers');

        // Clean the provided strings
        $providers = preg_replace('/\s+/', '', $providers);
        $providers = explode(',', $providers);
        $providers = array_filter($providers);

        // Make sure at least 2 providers as provided in order to continue
        if(count($providers) >= 2) {
            foreach($providers as $provider) {
                $ipsum[] = IpsumFacade::getText($provider, 10);
            }

            return response()->json([
                'similarities' => SimilarityAlgorithmFacade::getSimilarity(...$ipsum)
            ], 200);
        }

        return response()->json([
            'message' => 'Please specify at least 2 ipsum providers. Available providers: ' . implode(', ', IpsumFacade::getAllowedProviders())
        ], 400);
    }
}
