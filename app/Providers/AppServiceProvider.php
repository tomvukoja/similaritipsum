<?php

namespace App\Providers;

use App\Services\SimilarityAlgorithm\Algorithm;
use App\Services\Ipsum\Ipsum;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ipsum', function () {
            return new Ipsum();
        });

        $this->app->bind('similarity_algorithm', function() {
            return new Algorithm();
        });
    }
}
